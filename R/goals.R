#' @title Upload Goals
#'
#' @description
#' Upload Goals information.
#'
#' @param config PAth to config file
#'
#' @import DBI
#' @import data.table
#' @import jsonlite
#' @import yametrika
#'
#' @export
#'
update_goals <- function(config = "config.yml") {
  load_config(config)

  lg$info("Get 'goals' data from Yandex.Metrika API")
  goals <- setDT(ym_goal_list(.conf$counter_id, useDeleted = TRUE))

  lg$debug("Post process 'goals' data")
  conditions <- as.name("conditions")
  conditions_json <- as.name("conditions_json")
  is_retargeting <- as.name("is_retargeting")
  prev_goal_id <- as.name("prev_goal_id")
  id <- as.name("id")

  goals[, conditions_json := toJSON(conditions[[1]], auto_unbox = TRUE), by = id]
  goals[, conditions := conditions_json]
  goals[, conditions_json := NULL]
  goals[, is_retargeting := as.logical(is_retargeting)]
  goals[prev_goal_id == 0, prev_goal_id := NA]

  tbl_id <- Id(schema = .conf$db$schema, table = paste0(.conf$db$tables_prefix, "goals"))
  tbl_id_q <- dbQuoteIdentifier(.conf$db$con, tbl_id)

  lg$info("Write 'goals' data to {tbl_id_q}")
  dbWriteTable(
    conn = .conf$db$con,
    name = tbl_id,
    value = goals,
    overwrite = TRUE,
    row.names = FALSE,
    field.types = c(
      "id" = "BIGINT PRIMARY KEY",
      "prev_goal_id" = "BIGINT",
      "conditions" = "jsonb"
    )
  )

  return(invisible(TRUE))
}
