CREATE VIEW "{schema}"."{prefix}visits_goals" AS
SELECT visit_id,
       client_id,
       client_uuid,
       unnest(goals_id) AS goal_id,
       unnest(goals_date_time) AS goal_date_time
  FROM "{schema}"."{prefix}visits";

CREATE VIEW "{schema}"."{prefix}visits_watches" AS
SELECT visit_id,
       client_uuid,
       unnest(watch_ids) AS watch_id
  FROM "{schema}"."{prefix}visits";

CREATE VIEW "{schema}"."{prefix}first_visits" AS
SELECT DISTINCT ON (client_uuid)
       client_uuid AS client_uuid,
       visit_id,
       date_time
  FROM "{schema}"."{prefix}visits"
 ORDER BY client_uuid, visit_id;
