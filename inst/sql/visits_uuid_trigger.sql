CREATE OR REPLACE FUNCTION "{schema}"."gen_{prefix}visits_uuids"()
RETURNS TRIGGER AS $$
BEGIN
    IF NEW.client_id IS NOT NULL AND NEW.client_uuid IS NULL THEN
       NEW.client_uuid := md5(NEW.client_id::text)::uuid;
    END IF;
    RETURN new;
END
$$ LANGUAGE plpgsql;

 CREATE TRIGGER "insert_uuids"
 BEFORE INSERT
     ON "{schema}"."{prefix}visits" FOR EACH ROW
EXECUTE PROCEDURE "{schema}"."gen_{prefix}visits_uuids"();
