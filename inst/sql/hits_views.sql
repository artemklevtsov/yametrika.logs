CREATE VIEW "{schema}"."{prefix}hits_goals" AS
SELECT watch_id,
       client_id,
       client_uuid,
       unnest(goals_id) AS goal_id,
       date_time
  FROM "{schema}"."{prefix}hits";

CREATE VIEW "{schema}"."{prefix}hits_visits" AS
SELECT client_id,
       client_uuid,
       watch_id,
       date_time,
       md5(concat(client_id, min(watch_id) OVER w))::uuid AS visit_uuid,
       min(date_time) OVER w AS visit_start_data_time,
       max(date_time) OVER w AS visit_end_data_time
  FROM (SELECT client_uuid,
               client_id,
               watch_id,
               date_time,
               sum(delay) OVER w + 1 AS visit_count
          FROM (SELECT client_uuid,
                       client_id,
                       watch_id,
                       date_time,
                       CASE WHEN date_time - lag(date_time) OVER w > INTERVAL '30 minutes'
                            THEN 1 ELSE 0 END AS delay
                  FROM "{schema}"."{prefix}hits" AS ht
                WINDOW w AS (PARTITION BY client_uuid ORDER BY date_time)
               ) AS d
        WINDOW w AS (PARTITION BY client_uuid ORDER BY date_time)
       ) AS d
WINDOW w AS (PARTITION BY client_uuid, visit_count);
