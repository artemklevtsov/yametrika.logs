CREATE OR REPLACE FUNCTION "{schema}".gen_{colname}_uuid()
RETURNS TRIGGER AS $$
BEGIN
    IF NEW.{colname}_id IS NOT NULL AND NEW.client_uuid IS NULL THEN
       NEW.client_uuid := md5(NEW.{colname}_id)::uuid;
    END IF;
    RETURN new;
END
$$ LANGUAGE plpgsql
